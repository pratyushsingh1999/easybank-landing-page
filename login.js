window.addEventListener('load' , () => {
    const emailObj = document.getElementById('email');
    const passwordObj = document.getElementById('password');
    const buttonObj = document.getElementById('loginButton')
    let emailValid = false;
    let passwordValid = false;
    let loginButtonFlag = false;

    const validateEmail = (email) => {
        return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email);
    }
    const validatePasword = (pass) => {
        if (pass.length<1) { return false; }else { return true; }
    }
    const toggleButtonState = (flag) => {
        if (flag) {
            buttonObj.classList.remove('disabled');
        }else {
            buttonObj.classList.add('disabled');
        }
    }
    emailObj.addEventListener('input' , () => {
        emailValid = validateEmail(emailObj.value);
        if (emailValid && passwordValid) {
            loginButtonFlag = true;
            toggleButtonState(true);
        }else {
            loginButtonFlag = false;
            toggleButtonState(false);
        }
    })
    passwordObj.addEventListener('input' , () => {
        passwordValid = validatePasword(passwordObj.value);
        console.log(passwordValid);
        if (emailValid && passwordValid) {
            loginButtonFlag = true;
            toggleButtonState(true);
        }else {
            loginButtonFlag = false;
            toggleButtonState(false);
        }
    })
})
