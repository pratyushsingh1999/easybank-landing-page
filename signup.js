window.addEventListener('load' , () => {
    const fnameObj = document.getElementById('firstname');
    const lnameObj = document.getElementById('lastname');
    const emailObj = document.getElementById('email');
    const signupButtonObj = document.getElementById('signupButton');
    const passwordObj = document.getElementById('password');
    const passwordCheckObj = document.getElementById('passwordcheck');

    let fnameFlag = false;
    let lnameFlag = false;
    let emailFlag = false;
    let passwordFlag = false;

    const validateEmail = (email) => {
        return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email);
    }
    const validatePasword = (pass) => {
        if (pass.length<1) {
            return false;
        }else {
            return true;
        }
    }
    const showSignUpButton = () => {
        if (fnameFlag && lnameFlag && passwordFlag && emailFlag) {
            signupButtonObj.classList.remove('disabled');
        }else {
            signupButtonObj.classList.add('disabled');
        }
    }

    fnameObj.addEventListener('input' , () => {
        if (fnameObj.value.length<1) {
            fnameFlag = false;
        }else {
            fnameFlag = true;
        }
        showSignUpButton();
    })
    lnameObj.addEventListener('input', () => {
        if (lnameObj.value.length<1) {
            lnameFlag = false;
        }else {
            lnameFlag = true;
        }
        showSignUpButton();
    })
    emailObj.addEventListener('input', () => {
        if (validateEmail(emailObj.value)) {
            emailFlag = true;
        }else {
            emailFlag = false;
        }
        showSignUpButton();
    })
    passwordObj.addEventListener('input', () => {
        if (passwordObj.value === passwordCheckObj.value) {
            passwordFlag = true;
        }else {
            passwordFlag = false;
        }
        showSignUpButton();
    })
    passwordCheckObj.addEventListener('input', () => {
        if (passwordObj.value === passwordCheckObj.value) {
            passwordFlag = true;
        }else {
            passwordFlag = false;
        }
        showSignUpButton();
    })
})
