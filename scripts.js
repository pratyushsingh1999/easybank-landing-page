window.addEventListener('load' , () => {
    let flag = true;
    console.log(flag);
    const openNavPopup = document.getElementById('open-nav-popup');
    const navPopup = document.getElementById('nav-popup');
    const iconSwitcher = document.getElementById('icon-switcher');
    openNavPopup.addEventListener('click' , () => {
        console.log("clicked")
        if (flag) {
            flag = false;
            navPopup.style.setProperty('display' , 'flex');
            iconSwitcher.src = "images/icon-close.svg";
        }else {
            flag = true;
            navPopup.style.setProperty('display' , 'none');
            iconSwitcher.src = "images/icon-hamburger.svg"
        }
    })
})
