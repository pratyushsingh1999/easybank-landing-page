window.addEventListener('load' , () => {
    const emailObj = document.getElementById('email');
    const passwordObj = document.getElementById('pass');
    const buttonObj = document.getElementById('loginButton');
    const emailError = document.getElementById('email-err');
    const passError = document.getElementById('pass-err');
    let emailValid = false;
    let passwordValid = false;
    let loginButtonFlag = false;

    const validateEmail = (email) => {
        return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email);
    }
    const validatePasword = (pass) => {
        if (pass.length<1) { return false; }else { return true; }
    }
    const toggleButtonState = (flag) => {
        if (flag) {
            buttonObj.classList.remove('disabled');
        }else {
            buttonObj.classList.add('disabled');
        }
    }
    emailObj.addEventListener('input' , () => {
        emailValid = validateEmail(emailObj.value);
        if (emailValid) {
            emailError.classList.add('hide');
        }else {
            emailError.classList.remove('hide');
        }
        if (emailValid && passwordValid) {
            loginButtonFlag = true;
            toggleButtonState(true);
        }else {
            loginButtonFlag = false;
            toggleButtonState(false);
        }
    })
    passwordObj.addEventListener('input' , () => {
        passwordValid = validatePasword(passwordObj.value);
        if (passwordValid) {
            passError.classList.add('hide');
        }else {
            passError.classList.remove('hide');
        }
        console.log(passwordValid);
        if (emailValid && passwordValid) {
            loginButtonFlag = true;
            toggleButtonState(true);
        }else {
            loginButtonFlag = false;
            toggleButtonState(false);
        }
    })
})
